/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <inttypes.h>
#include <semaphore.h>
#include <unistd.h>
#include <securec.h>

#ifdef __cplusplus
extern "C" {
#endif

#define MAC_SIZE 6
#define PKG_NAME "com.huawei.dmsdp"

#define MAC_STR "%02x:%02x:%02x:%02x:%02x:%02x"
#define MAC_ADDR(a) (a)[0], (a)[1], (a)[2], (a)[3], (a)[4], (a)[5]

inline void WaitInputEnter(const char *info)
{
    printf("==>%s", info);
    fflush(stdout);
    fflush(stdin);
    (void)getchar();
}

inline int32_t GetInputNumber(const char *info)
{
    int32_t num;
    printf("==>%s", info);
    fflush(stdout);
    fflush(stdin);
    (void)scanf_s("%d", &num, sizeof(num));
    return num;
}

inline void GetInputString(const char *info, char *str, uint32_t size)
{
    printf("==>%s", info);
    fflush(stdout);
    fflush(stdin);
    if (scanf_s("%s", str, size) < 0) {
        printf("[ERR]invalid input!\n");
    }
}

#ifdef __cplusplus
}
#endif
#endif