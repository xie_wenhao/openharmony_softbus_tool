/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "bus_center.h"
#include "common.h"
#include "discover.h"
#include "transmission.h"

#define CMD_DESC_INFO_LEN 32

typedef void(*CmdFunc)(void);
typedef struct {
    char desc[CMD_DESC_INFO_LEN];
    CmdFunc func;
} CmdNode;

static void Exit(void);

static CmdNode g_cmdList[] = {
    { "PublishService", D_PublishService },
    { "UnPublishService", D_UnPublishService },
    { "StartDiscovery", D_StartDiscovery },
    { "StopDiscovery", D_StopDiscovery },
    { "JoinLNN", BC_JoinLNN },
    { "LeaveLNN", BC_LeaveLNN },
    { "GetLocalDeviceInfo", BC_GetLocalDeviceInfo },
    { "GetOnlineDeviceInfo", BC_GetOnlineDeviceInfo },
    { "GetBtMac", D_GetBtMac },
    { "CreateSessionServer", T_CreateSessionServer },
    { "RemoveSessionServer", T_RemoveSessionServer },
    { "OpenSession", T_OpenSession },
    { "CloseSession", T_CloseSession },
    { "SetFileSendListener", T_SetFileSendListener },
    { "SetFileRecvListener", T_SetFileReceiveListener },
    { "SendMessage", T_SendMessage },
    { "SendBytes", T_SendBytes },
    { "SendStream", T_SendStream },
    { "SendFile", T_SendFile },
    { "Exit", Exit }, /* keep Exit at last */
};
static unsigned int g_cmdNum = sizeof(g_cmdList) / sizeof(CmdNode);

static void Exit(void)
{
    printf("BYE!\n");
}

static void Helper(void)
{
    printf("******Softbus Tool Command List******\n");
    for (unsigned int i = 0; i < g_cmdNum; i++) {
        printf("*     %02d - %-20s     *\r\n", i, g_cmdList[i].desc);
    }
    printf("*************************************\n");
}

int main(void)
{
    unsigned int index;
    do {
        Helper();
        index = GetInputNumber("Please input cmd index:");
        if (index < 0 || index >= g_cmdNum) {
            printf("invalid cmd:%d.\n", index);
            continue;
        }
        printf("\nExecute: %s\n", g_cmdList[index].desc);
        g_cmdList[index].func();
    } while ((index + 1) != g_cmdNum);

    return 0;
}